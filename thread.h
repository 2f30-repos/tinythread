/* See LICENSE file for copyright and license details. */
#ifndef THREADLIB_H
#define THREADLIB_H

#include <sched.h>
#include <stddef.h>

struct thread_config {
	size_t stack_size;
	size_t guard_size;
};

typedef int spinlock_t;

struct thread_ctx;

struct thread_ctx *thread_init(const struct thread_config *tc, int *rval);
pid_t thread_register(struct thread_ctx *tctx, int (*fn)(void *), void *arg);
int thread_wait(struct thread_ctx *tctx, pid_t pid);
int thread_wait_blocking(struct thread_ctx *tctx, pid_t pid);
int thread_wait_all_blocking(struct thread_ctx *tctx);
void thread_exit(struct thread_ctx *tctx);
pid_t thread_id(void);
int thread_get_sched_param(struct thread_ctx *tctx, pid_t pid, int *policy,
			   struct sched_param *param);
int thread_set_sched_param(struct thread_ctx *tctx, pid_t pid, int policy,
			   const struct sched_param *param);

static inline void
spinlock_init(spinlock_t *spinlock)
{
	__sync_lock_release(spinlock);
}

static inline void
acquire(spinlock_t *spinlock)
{
	while (__sync_lock_test_and_set(spinlock, 1) == 1)
		;
}

static inline int
try_acquire(spinlock_t *spinlock)
{
	return __sync_lock_test_and_set(spinlock, 1);
}

static inline void
release(spinlock_t *spinlock)
{
	__sync_lock_release(spinlock);
}

#endif
