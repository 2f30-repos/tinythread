VERSION = 0.1

# paths
PREFIX = /usr/local
CC = cc
# flags
CPPFLAGS = -D_GNU_SOURCE
CFLAGS = -fPIC -Wall -O3 ${CPPFLAGS}
LDFLAGS = -static
