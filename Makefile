# See LICENSE file for copyright and license details.

include config.mk

SRC = thread.c
OBJ = $(SRC:.c=.o)

LIB = libthread.a
INC = thread.h

all: $(LIB) threadtest

$(LIB): $(OBJ)
	$(AR) -rcs $@ $(OBJ)

threadtest: threadtest.o $(LIB)
	$(CC) $(LDFLAGS) -o $@ threadtest.o $(LIB)

.c.o:
	$(CC) $(CFLAGS) -c $<

install: $(LIB) $(INC) $(MAN)
	@echo @ install libthread to $(DESTDIR)$(PREFIX)
	@mkdir -p $(DESTDIR)$(PREFIX)/lib
	@cp $(LIB) $(DESTDIR)$(PREFIX)/lib/$(LIB)
	@mkdir -p $(DESTDIR)$(PREFIX)/include
	@cp $(INC) $(DESTDIR)$(PREFIX)/include/$(INC)

uninstall:
	@echo @ uninstall libthread from $(DESTDIR)$(PREFIX)
	@rm -f $(DESTDIR)$(PREFIX)/lib/$(LIB)
	@rm -f $(DESTDIR)$(PREFIX)/include/$(INC)

clean:
	rm -f $(LIB) threadtest threadtest.o $(OBJ)
