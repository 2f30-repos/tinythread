/* See LICENSE file for copyright and license details. */
#include <sys/wait.h>
#include <sys/mman.h>
#include <sched.h>
#include <unistd.h>
#include <errno.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "list.h"
#include "thread.h"

struct thread {
	struct list_head list;
	void *stack;
	pid_t pid;
	int (*fn)(void *);
	void *args;
};

struct thread_ctx {
	/* Configuration parameters for the library */
	struct thread_config thread_config;
	/* Lock used internally to ensure the core is protected */
	spinlock_t lock;
	/* List of all registered threads */
	struct list_head thread_list;
};

struct thread_ctx *
thread_init(const struct thread_config *tc, int *rval) {
	struct thread_ctx *tctx;
	long page_size;
	size_t stack_size;
	size_t guard_size;

	tctx = calloc(1, sizeof *tctx);
	if (!tctx) {
		if (rval)
			*rval = -ENOMEM;
		return NULL;
	}

	spinlock_init(&tctx->lock);
	INIT_LIST_HEAD(&tctx->thread_list);

	page_size = sysconf(_SC_PAGESIZE);
	if (page_size < 0)
		page_size = 4096;

	if (tc) {
		/* Ensure stack size and guard size are
		 * page aligned */
		stack_size = (tc->stack_size +
			      (page_size - 1)) & ~(page_size - 1);
		guard_size = (tc->guard_size +
			      (page_size - 1)) & ~(page_size - 1);
		tctx->thread_config.stack_size = stack_size;
		tctx->thread_config.guard_size = guard_size;
	} else {
		tctx->thread_config.stack_size = page_size * 16;
		tctx->thread_config.guard_size = page_size;
	}

	return tctx;
}

pid_t
thread_register(struct thread_ctx *tctx, int (*fn)(void *), void *arg)
{
	struct thread *t;
	pid_t ret;

	if (!tctx)
		return -EINVAL;

	acquire(&tctx->lock);
	t = malloc(sizeof *t);
	if (!t) {
		release(&tctx->lock);
		return -ENOMEM;
	}

	t->stack = mmap(0, tctx->thread_config.stack_size, PROT_READ | PROT_WRITE,
			MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
	if (t->stack == MAP_FAILED) {
		ret = -errno;
		goto free_thread;
	}

	/* Set the guard page at the end of the stack */
	ret = mprotect(t->stack, tctx->thread_config.guard_size, PROT_NONE);
	if (ret < 0) {
		ret = -errno;
		goto free_stack;
	}

	t->pid = clone(fn, (char *)t->stack + tctx->thread_config.stack_size,
		       SIGCHLD | CLONE_FS | CLONE_FILES | CLONE_SIGHAND |
		       CLONE_VM | CLONE_SYSVSEM, arg, NULL, NULL, NULL);
	if (t->pid < 0) {
		ret = -errno;
		goto free_stack;
	}

	t->fn = fn;
	t->args = arg;

	list_add_tail(&t->list, &tctx->thread_list);
	release(&tctx->lock);

	return t->pid;

free_stack:
	munmap(t->stack, tctx->thread_config.stack_size);
free_thread:
	free(t);
	release(&tctx->lock);
	return ret;
}

/* Called with lock acquired */
static void
thread_remove(struct thread_ctx *tctx, pid_t pid)
{
	struct list_head *iter, *q;
	struct thread *tmp;

	/* A hash table here would be better for many threads */
	list_for_each_safe(iter, q, &tctx->thread_list) {
		tmp = list_entry(iter, struct thread, list);
		if (tmp->pid == pid) {
			list_del(&tmp->list);
			munmap(tmp->stack, tctx->thread_config.stack_size);
			free(tmp);
			break;
		}
	}
}

int
thread_wait(struct thread_ctx *tctx, pid_t pid)
{
	pid_t p;
	int status;

	if (!tctx)
		return -EINVAL;

	acquire(&tctx->lock);
	p = wait(&status);
	if (p < 0) {
		release(&tctx->lock);
		return -errno;
	}
	if (p == pid) {
		thread_remove(tctx, p);
		release(&tctx->lock);
		return WEXITSTATUS(status);
	}
	release(&tctx->lock);

	return -EAGAIN;
}

int
thread_wait_blocking(struct thread_ctx *tctx, pid_t pid)
{
	pid_t p;
	int status;

	if (!tctx)
		return -EINVAL;

	acquire(&tctx->lock);
	while (1) {
		p = wait(&status);
		if (p < 0) {
			release(&tctx->lock);
			return -errno;
		}
		if (p == pid) {
			thread_remove(tctx, p);
			break;
		}
	}
	release(&tctx->lock);

	return WEXITSTATUS(status);
}

int
thread_wait_all_blocking(struct thread_ctx *tctx)
{
	pid_t pid;

	if (!tctx)
		return -EINVAL;

	acquire(&tctx->lock);
	while (!list_empty(&tctx->thread_list)) {
		pid = wait(NULL);
		if (pid < 0) {
			release(&tctx->lock);
			return -errno;
		}
		thread_remove(tctx, pid);
	}
	release(&tctx->lock);

	return 0;
}

void
thread_exit(struct thread_ctx *tctx)
{
	struct list_head *iter, *q;
	struct thread *tmp;

	if (!tctx)
		return;

	acquire(&tctx->lock);
	while (!list_empty(&tctx->thread_list)) {
		/* A hash table here would be better for many threads */
		list_for_each_safe(iter, q, &tctx->thread_list) {
			tmp = list_entry(iter, struct thread, list);
			list_del(&tmp->list);
			munmap(tmp->stack, tctx->thread_config.stack_size);
			free(tmp);
		}
	}
	release(&tctx->lock);
	free(tctx);
}

pid_t
thread_id(void)
{
	return getpid();
}

int
thread_get_sched_param(struct thread_ctx *tctx, pid_t pid, int *policy,
		       struct sched_param *param)
{
	struct list_head *iter;
	struct thread *tmp;
	int ret = -EINVAL;

	if (!tctx)
		return -EINVAL;

	acquire(&tctx->lock);
	list_for_each(iter, &tctx->thread_list) {
		tmp = list_entry(iter, struct thread, list);
		if (tmp->pid == pid) {
			ret = sched_getparam(tmp->pid, param);
			if (ret < 0)
				ret = -errno;
			else
				*policy = sched_getscheduler(tmp->pid);
			break;
		}
	}
	release(&tctx->lock);

	return ret;
}

int
thread_set_sched_param(struct thread_ctx *tctx, pid_t pid, int policy,
		       const struct sched_param *param)
{
	struct list_head *iter;
	struct thread *tmp;
	int ret = -EINVAL;

	if (!tctx)
		return -EINVAL;

	acquire(&tctx->lock);
	list_for_each(iter, &tctx->thread_list) {
		tmp = list_entry(iter, struct thread, list);
		if (tmp->pid == pid) {
			ret = sched_setscheduler(tmp->pid, policy, param);
			if (ret < 0)
				ret = -errno;
			break;
		}
	}
	release(&tctx->lock);

	return ret;
}
