/* See LICENSE file for copyright and license details. */
#include <unistd.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "thread.h"

enum {
	THREADS_SPAWN_NR = 64
};

/* This gets incremented once by each thread */
static int counter;
/* Lock that protects `counter' */
static spinlock_t lock;
/* Per thread arguments */
static struct thread_args {
	int id;
} thread_args[THREADS_SPAWN_NR];

static int
foo(void *arg)
{
	struct thread_args *args = arg;

	acquire(&lock);
	printf("Entered thread with id %d\n", args->id);
	counter++;
	release(&lock);

	return 0;
}

int
main(void)
{
	int i;
	struct thread_ctx *tctx;

	setbuf(stdout, NULL);

	tctx = thread_init(NULL, NULL);
	spinlock_init(&lock);

	for (i = 0; i < THREADS_SPAWN_NR; i++) {
		thread_args[i].id = i;
		thread_register(tctx, foo, &thread_args[i]);
	}

	thread_wait_all_blocking(tctx);

	thread_exit(tctx);

	return EXIT_SUCCESS;
}
